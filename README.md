# MR verification

This repository only serves to allow code role applicants to verify their ownership of a github account.

If you are attempting to verify:
1. [Create a new issue](https://gitlab.com/mlengy/mr-verification/-/issues/new) in this repository.
2. Set the title to your full Discord username (i.e. `foo#0001`).
3. Add either a `proficient` or `expert` label depending on the nature of your application.
3. Leave all other fields blank.

An example can be found [here](https://gitlab.com/mlengy/mr-verification/-/issues/1).
